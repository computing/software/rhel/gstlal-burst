%define gstreamername gstreamer1
%global __python %{__python3}

Name: gstlal-burst
Version: 0.4.0
Release: 1.1%{?dist}
Summary: GSTLAL Burst
License: GPL
Group: LSC Software/Data Analysis

# --- package requirements --- #
Requires: fftw >= 3
Requires: gobject-introspection >= 1.30.0
Requires: gstlal >= 1.10.0
Requires: gstlal-ugly >= 1.10.0
Requires: %{gstreamername} >= 1.14.1
Requires: %{gstreamername}-plugins-base >= 1.14.1
Requires: %{gstreamername}-plugins-good >= 1.14.1
Requires: gsl
Requires: orc >= 0.4.16

# --- LSCSoft package requirements --- #
Requires: lal >= 7.2.4
Requires: lalmetaio >= 3.0.2
Requires: lalburst >= 1.7.0
Requires: python%{python3_pkgversion}-ligo-segments >= 1.2.0
Requires: python%{python3_pkgversion}-ligo-scald >= 0.7.2

# --- python package requirements --- #
Requires: python3 >= 3.6
Requires: python%{python3_pkgversion}-%{gstreamername}
Requires: python%{python3_pkgversion}-h5py
Requires: python%{python3_pkgversion}-numpy
Requires: python%{python3_pkgversion}-scipy

%if 0%{?rhel} == 8
Requires: python%{python3_pkgversion}-numpy >= 1.7.0
Requires: python3-matplotlib
%else
Requires: numpy >= 1.7.0
Requires: python%{python3_pkgversion}-matplotlib
%endif

# -- build requirements --- #
BuildRequires: fftw-devel >= 3
BuildRequires: gobject-introspection-devel >= 1.30.0
BuildRequires: graphviz
BuildRequires: gsl-devel
BuildRequires: gstlal-devel >= 1.10.0
BuildRequires: %{gstreamername}-devel >= 1.14.1
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.14.1
BuildRequires: liblal-devel >= 7.2.4
BuildRequires: liblalburst-devel >= 1.7.0
BuildRequires: liblalmetaio-devel >= 3.0.2
BuildRequires: orc >= 0.4.16
BuildRequires: pkgconfig >= 0.18.0
BuildRequires: python3-devel >= 3.6
BuildRequires: python%{python3_pkgversion}-lal >= 7.2.4
Conflicts: gstlal-ugly < 0.6.0
Source: https://software.igwn.org/lscsoft/source/gstlal-burst-%{version}.tar.gz
URL: https://git.ligo.org/lscsoft/gstlal
Packager: Patrick Godwin <patrick.godwin@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package contains the plugins and shared libraries required to run the gstlal burst (generic transient) pipeline.

%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version} 
Requires: gstlal-devel >= 1.10.0
Requires: gstlal-ugly-devel >= 1.10.0
Requires: python3-devel >= 3.6 
Requires: fftw-devel >= 3 
Requires: %{gstreamername}-devel >= 1.14.1
Requires: %{gstreamername}-plugins-base-devel >= 1.14.1 
Requires: liblal-devel >= 7.2.4
Requires: liblalmetaio-devel >= 3.0.2
Requires: liblalburst-devel >= 1.7.0

%description devel
This package contains the files needed for building gstlal-burst based plugins
and programs.

%prep
%setup -q -n %{name}-%{version}


%build
%configure PYTHON=python3
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal
%{_libdir}/gstreamer-1.0/*.so
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_libdir}/gstreamer-1.0/*.a
%{_includedir}/*
